"use strict"

module.exports = (event, context) => {
  context
    .headers({"Content-Type": "application/json"})
    .status(200)
    .succeed({title:"Hello Laurent 😃"})
}
