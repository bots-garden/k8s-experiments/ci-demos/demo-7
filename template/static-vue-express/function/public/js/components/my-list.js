
Vue.component(`my-list`, {
  template: `
    <table class="u-full-width">
      <thead>
        <tr>
          <th>{{column1}}</th>
          <th>{{column2}}</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="buddy in buddies">
          <td><h2>{{buddy.avatar}}</h2></td>
          <td>{{buddy.name}}</td>
        </tr>
      </tbody>
    </table>
  `,
  data() {
    return {
      column1: "Avatar",
      column2: "Name",
      buddies: null
    }
  },
  methods: {
    populateTheList: function() {
      this.buddies = [
          {avatar: "🦊", name: "Foxy"}
        , {avatar: "🦁", name: "Leo"}
        , {avatar: "🐯", name: "Tigrou"}
      ]
    }
  },
  mounted() {
    console.log("⚡️ the my-list vue is mounted")
    this.populateTheList()
    this.$root.$emit("ping", "my-list")
  }
})
