// Copyright (c) @k33g_org 2019. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

"use strict"

const express = require('express')
const bodyParser = require("body-parser")
const app = express()

const port = process.env.http_port || 3000

app.use(express.static('./function/public'))
app.use(express.json())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))


app.listen(port, () => {
  console.log(`👋 OpenFaaS is listening on port ${port} 😃`)
  console.log(`🌍 http://localhost:${port}`)
})

